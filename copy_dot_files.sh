#!/bin/bash 

cp ~/.vimrc ./vimrc_pc_khanal
cp ~/.aliases ./aliases_pc_khanal
scp khanal@rupc08.rutgers.edu:~/.aliases ./aliases_cluster_khanal
scp khanal@rupc08.rutgers.edu:~/.vimrc ./vimrc_cluster_khanal
